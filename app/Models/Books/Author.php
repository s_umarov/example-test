<?php

namespace App\Models\Books;

use App\Services\Enums\GenderEnum;
use App\Services\Traits\UserStamp\CreatorAndUpdater;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use CreatorAndUpdater;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'authors';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    // protected $hidden = [];
    // protected $fakeColumns = [];
    protected $dates = [
        'birth_date'
    ];
    // protected $casts = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Get model all books relation instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books()
    {
        return $this->hasMany(Book::class, 'author_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /**
     * Scope author age between clause query
     *
     * @param Builder $query
     * @param $ageStart
     * @param null $ageEnd
     * @return Builder
     */
    public function scopeWithAgeBetween(Builder $query, $ageStart, $ageEnd = null)
    {
        $ageEnd = $ageEnd ?? $ageStart;

        $dateBegin = now()->subYears($ageEnd)->startOfDay()->toDateTimeString();
        $dateEnd = now()->subYears($ageStart)->endOfDay()->toDateTimeString();

        return $query->whereBetween('birth_date', [$dateBegin, $dateEnd]);
    }

    /**
     * Scope with author ids clause query
     *
     * @param Builder $query
     * @param array $ids
     * @return Builder
     */
    public function scopeWithAuthorIds(Builder $query, array $ids)
    {
        if (count($ids)) {
            return $query->whereIn('id', $ids);
        }

        return $query;
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /**
     * Get author gender name
     *
     * @return mixed|null
     */
    public function getGenderNameAttribute()
    {
        return GenderEnum::getTitle($this->gender);
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
