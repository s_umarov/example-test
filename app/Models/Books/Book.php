<?php

namespace App\Models\Books;

use App\Services\Traits\UserStamp\CreatorAndUpdater;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use CreatorAndUpdater;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'books';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    // protected $hidden = [];
    // protected $fakeColumns = [];
    // protected $dates = [];
    // protected $casts = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /**
     * Get author relation instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(Author::class, 'author_id', 'id');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /**
     * Scope with author age between
     *
     * @param Builder $query
     * @param $startAge
     * @param null $endAge
     * @return Builder
     */
    public function scopeWithAuthorAged(Builder $query, $startAge, $endAge = null)
    {
        return $query->whereHas('author', function ($query) use ($startAge, $endAge) {
            $query->withAgeBetween($startAge, $endAge);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /**
     * Get model author name
     *
     * @return string|null
     */
    public function getAuthorNameAttribute()
    {
        return optional($this->author)->name;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
