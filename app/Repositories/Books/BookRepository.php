<?php

namespace App\Repositories\Books;

use App\Interfaces\Books\BookRepositoryInterface;
use App\Interfaces\CrudInterface;
use App\Models\Books\Book;

class BookRepository implements CrudInterface, BookRepositoryInterface
{
    /**
     * Define model variable
     *
     * @var Book
     */
    protected $model;

    /**
     * BookRepository constructor.
     * @param Book $book
     */
    public function __construct(Book $book)
    {
        $this->model = $book;
    }

    /**
     * Get all model list
     *
     * @return Book[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllItems()
    {
        return $this->model->all();
    }

    /**
     * Get model by id or throw not found exception
     *
     * @param $bookId
     * @return mixed
     */
    public function getById($bookId)
    {
        return $this->model->findOrFail($bookId);
    }

    /**
     * Create new model instance
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update model properties
     *
     * @param $bookId
     * @param array $data
     * @return mixed
     */
    public function update($bookId, array $data)
    {
        return $this->getById($bookId)->update($data);
    }

    /**
     * Delete model instance
     *
     * @param $bookId
     * @return mixed
     */
    public function delete($bookId)
    {
        return $this->getById($bookId)->delete();
    }

    /**
     * Get books based on authors age
     *
     * @param $startAge
     * @param null $endAge
     * @return mixed
     */
    public function getBooksWithAuthorAge($startAge, $endAge = null)
    {
        return $this->model->withAuthorAged($startAge, $endAge)->get();
    }
}