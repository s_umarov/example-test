<?php

namespace App\Repositories\Books;

use App\Interfaces\Books\AuthorRepositoryInterface;
use App\Models\Books\Author;
use App\Models\Books\Book;

class AuthorRepository implements AuthorRepositoryInterface
{
    /**
     * Define model variable
     *
     * @var Book
     */
    protected $model;

    /**
     * BookRepository constructor.
     * @param Author $author
     */
    public function __construct(Author $author)
    {
        $this->model = $author;
    }

    /**
     * Get all model list
     *
     * @return Book[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllItems()
    {
        return $this->model->all();
    }

    /**
     * Get model by id or throw not found exception
     *
     * @param $authorId
     * @return mixed
     */
    public function getById($authorId)
    {
        return $this->model->findOrFail($authorId);
    }

    /**
     * Create new model instance
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    /**
     * Update model properties
     *
     * @param $authorId
     * @param array $data
     * @return mixed
     */
    public function update($authorId, array $data)
    {
        return $this->getById($authorId)->update($data);
    }

    /**
     * Delete model instance
     *
     * @param $authorId
     * @return mixed
     */
    public function delete($authorId)
    {
        return $this->getById($authorId)->delete();
    }

    /**
     * Get authors all books
     *
     * @param $authorId
     * @return mixed
     */
    public function getAllBooks($authorId)
    {
        return $this->getById($authorId)->books;
    }
}