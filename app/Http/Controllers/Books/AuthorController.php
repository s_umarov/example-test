<?php

namespace App\Http\Controllers\Books;

use App\Http\Controllers\Controller;
use App\Http\Requests\Books\StoreAuthorRequest;
use App\Http\Requests\Books\StoreBookRequest;
use App\Http\Requests\Books\UpdateAuthorRequest;
use App\Http\Requests\Books\UpdateBookRequest;
use App\Http\Resources\Books\AuthorResource;
use App\Http\Resources\Books\BookResource;
use App\Interfaces\Books\AuthorRepositoryInterface;
use App\Interfaces\Books\BookRepositoryInterface;

class AuthorController extends Controller
{
    /**
     * Define repository variable
     *
     * @var BookRepositoryInterface
     */
    private $authorRepository;

    /**
     * BookController constructor.
     * @param AuthorRepositoryInterface $authorRepository
     */
    function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->authorRepository = $authorRepository;
    }

    /**
     * Get all books
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return AuthorResource::collection($this->authorRepository->getAllItems());
    }

    /**
     * Store new model instance
     *
     * @param StoreAuthorRequest $request
     * @return mixed
     */
    public function store(StoreAuthorRequest $request)
    {
        $this->authorRepository->create($request->validated());

        return response()->noContent();
    }

    /**
     * Show specific model instance
     *
     * @param $id
     * @return AuthorResource
     */
    public function show($id)
    {
        return AuthorResource::make($this->authorRepository->getById($id));
    }

    /**
     * Update specific model properties
     *
     * @param UpdateAuthorRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAuthorRequest $request, $id)
    {
        $this->authorRepository->update($id, $request->validated());

        return response()->noContent();
    }

    /**
     * Delete specific model instance
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorRepository->delete($id);

        return response()->noContent();
    }

    /**
     * Get author all books
     *
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function authorAllBooks($id)
    {
        return BookResource::collection($this->authorRepository->getAllBooks($id));
    }
}
