<?php

namespace App\Http\Controllers\Books;

use App\Http\Controllers\Controller;
use App\Http\Requests\Books\StoreBookRequest;
use App\Http\Requests\Books\UpdateBookRequest;
use App\Http\Resources\Books\BookResource;
use App\Interfaces\Books\BookRepositoryInterface;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Define repository variable
     *
     * @var BookRepositoryInterface
     */
    private $bookRepository;

    /**
     * BookController constructor.
     * @param BookRepositoryInterface $bookRepository
     */
    function __construct(BookRepositoryInterface $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    /**
     * Get all books
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return BookResource::collection($this->bookRepository->getAllItems());
    }

    /**
     * Store new model instance
     *
     * @param StoreBookRequest $request
     * @return mixed
     */
    public function store(StoreBookRequest $request)
    {
        $this->bookRepository->create($request->validated());

        return response()->noContent();
    }

    /**
     * Show specific model instance
     *
     * @param $id
     * @return BookResource
     */
    public function show($id)
    {
        return BookResource::make($this->bookRepository->getById($id));
    }

    /**
     * Update specific model properties
     *
     * @param UpdateBookRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookRequest $request, $id)
    {
        $this->bookRepository->update($id, $request->validated());

        return response()->noContent();
    }

    /**
     * Delete specific model instance
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->bookRepository->delete($id);

        return response()->noContent();
    }

    /**
     * Get list of books based on author age
     *
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getBooksBasedAuthorAge(Request $request)
    {
        return BookResource::collection($this->bookRepository->getBooksWithAuthorAge($request->start_age, $request->end_age));
    }
}
