<?php

namespace App\Http\Resources\Books;

use App\Http\Resources\UserRelationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->$this->updated_at,
            'creator' => UserRelationResource::make($this->creator),
            'updater' => UserRelationResource::make($this->updater),
            'name' => $this->name,
            'sex' => $this->sex_name,
            'birth_date' => $this->birth_date
        ];
    }
}
