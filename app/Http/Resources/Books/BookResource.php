<?php

namespace App\Http\Resources\Books;

use App\Http\Resources\UserRelationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BookResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->$this->updated_at,
            'creator' => UserRelationResource::make($this->creator),
            'updater' => UserRelationResource::make($this->updater),
            'name' => $this->code,
            'notes' => $this->name,
            'author' => AuthorResource::make($this->author)
        ];
    }
}
