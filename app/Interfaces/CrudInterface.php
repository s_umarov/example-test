<?php

namespace App\Interfaces;

interface CrudInterface
{
    public function getAllItems();
    public function getById($id);
    public function create(array $attributes);
    public function update($id, array $attributes);
    public function delete($id);
}