<?php

namespace App\Interfaces\Books;

use App\Interfaces\CrudInterface;

interface BookRepositoryInterface extends CrudInterface
{
    public function getBooksWithAuthorAge($dateBegin, $dateEnd = null);
}