<?php

namespace App\Interfaces\Books;

use App\Interfaces\CrudInterface;

interface AuthorRepositoryInterface extends CrudInterface
{
    public function getAllBooks($authorId);
}