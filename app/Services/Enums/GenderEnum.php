<?php

namespace App\Services\Enums;

class GenderEnum
{
    const MALE = 1;
    const FEMALE = 2;

    /**
     * @return array
     */
    public static function toArray(){
        return [
            self::MALE => 'Мужской',
            self::FEMALE => 'Женский'
        ];
    }

    /**
     * Get key value from array
     *
     * @param $key
     * @return mixed|null
     */
    public static function getTitle($key)
    {
        $items = self::toArray();

        return self::returnWithKeyValue($items, $key);
    }

    /**
     * Get key value from array
     *
     * @param $title
     * @return mixed|null
     */
    public static function getKey($title)
    {
        $items = array_flip(self::toArray());

        return self::returnWithKeyValue($items, $title);
    }

    /**
     * Form result value
     *
     * @param $array
     * @param $key
     * @return mixed|null
     */
    protected static function returnWithKeyValue($array, $key)
    {
        return array_key_exists($key, $array) ? $array[$key] : null;
    }
}
