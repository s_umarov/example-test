<?php


namespace App\Services\Traits\UserStamp;


trait CreatorAndUpdater
{
    use Creator, Updater;
}
