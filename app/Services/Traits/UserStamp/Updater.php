<?php

namespace App\Services\Traits\UserStamp;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait Updater
{
    /**
     * Bootstrap updater trait.
     *
     * @return void
     */
    public static function bootUpdater()
    {
        static::updating(function ($item) {
            $item->updater_id = optional(Auth::user())->id;
        });
    }

    /**
     * Get updater relation model instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }
}
