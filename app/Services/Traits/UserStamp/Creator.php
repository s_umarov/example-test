<?php

namespace App\Services\Traits\UserStamp;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait Creator
{
    /**
     * Bootstrap creator trait.
     *
     * @return void
     */
    public static function bootCreator()
    {
        static::creating(function ($item) {
            $item->creator_id = optional(Auth::user())->id;
        });
    }

    /**
     * Get creator relation model instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }
}
