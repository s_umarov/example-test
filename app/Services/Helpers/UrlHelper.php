<?php

namespace App\Services\Helpers;

class UrlHelper
{

    /**
     * Convert url params to array
     *
     * @param null $url
     * @return array
     */
    public static function urlParamsToArray($url = null)
    {
        $url = $url ?? request()->getQueryString();

        $requestParams = [];
        foreach (explode('&', $url) as $key => $value) {
            if ($value) {
                $parameter = explode('=', $value);
                $requestParams[$parameter[0]] = $parameter[1];
            }
        }

        return $requestParams;
    }

    /**
     * Generate new custom url
     *
     * @return string
     */
    public static function generateNewCustomUrl()
    {
        $params = self::urlParamsToArray();

        if (count($params)) {
            $params = array_diff($params, [10]);
            arsort($params);
            $params["url"] = urlencode(request()->getPathInfo());

            $newUrl = request()->getSchemeAndHttpHost() . "/?" . http_build_query($params);

            return $newUrl;
        }

        return request()->getUri();
    }
}
