<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable()->comment('Название автора');
            $table->integer('gender')->nullable()->comment('Пол');
            $table->date('birth_date')->nullable()->comment('Дата рождения');
            $table->unsignedBigInteger('creator_id')->index()->nullable()->comment('Создано пользователем');
            $table->unsignedBigInteger('updater_id')->index()->nullable()->comment('Изменено пользователем');
            $table->timestamps();

            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('updater_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
