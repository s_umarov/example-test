<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable()->comment('Название книги');
            $table->longText('notes')->nullable()->comment('Описание');
            $table->unsignedBigInteger('author_id')->index()->nullable()->comment('Автор');
            $table->unsignedBigInteger('creator_id')->index()->nullable()->comment('Создано пользователем');
            $table->unsignedBigInteger('updater_id')->index()->nullable()->comment('Изменено пользователем');
            $table->timestamps();

            $table->foreign('author_id')->references('id')->on('authors');
            $table->foreign('creator_id')->references('id')->on('users');
            $table->foreign('updater_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
